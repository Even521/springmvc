package com.spring.mvc.model;

public class JsonResult {
	private boolean success;
	private String msg;
	
	public JsonResult(boolean success,String msg){
		this.success = success;
		this.msg = msg;
	}
	public JsonResult(){
		
	}
	public boolean getSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}
}
