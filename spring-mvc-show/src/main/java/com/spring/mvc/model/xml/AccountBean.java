package com.spring.mvc.model.xml;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement(name = "account")
public class AccountBean {

	private int id;
	private String name;
	private String email;
	private String address;
	private Brithday brithday;

	@XmlElement
	public Brithday getBrithday() {

		return brithday;

	}

	public void setBrithday(Brithday brithday) {

		this.brithday = brithday;

	}

	@XmlElement
	public int getId() {

		return id;

	}

	public void setId(int id) {

		this.id = id;

	}

	@XmlElement
	public String getName() {

		return name;

	}

	public void setName(String name) {

		this.name = name;

	}

	@XmlElement
	public String getEmail() {

		return email;

	}

	public void setEmail(String email) {

		this.email = email;

	}

	@XmlElement
	public String getAddress() {

		return address;

	}

	public void setAddress(String address) {

		this.address = address;

	}

	@Override
	public String toString() {

		return this.name + "#" + this.id + "#" + this.address + "#" + this.brithday + "#" + this.email;

	}
}
