package com.spring.mvc.model;
import java.io.Serializable;
import java.util.Date;

/**
 * 实现序列化接口
 * 
 * @author damon
 * 
 */
public class User implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String userName;
	private String password;
	private String mobileNO;
	private String id;
	private String name;
	private String address;
	private String sex;
	private Date birthday;
	private int age;

	/**
	 * 此处必须提供一个空构造函数
	 */
	public User() {

	}
	

	public User(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}


	public User(String id, String name, String password, String address,
			String sex, int age) {
		this.id = id;
		this.name = name;
		this.password = password;
		this.address = address;
		this.sex = sex;
		this.age = age;
	}
	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMobileNO() {
		return mobileNO;
	}

	public void setMobileNO(String mobileNO) {
		this.mobileNO = mobileNO;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	@Override
	public String toString() {		
		return "name: " + name + ",password: " + password+",sex: " + sex + ",age: " + age+",address: " + address + ",id: " + id;
	}
}
