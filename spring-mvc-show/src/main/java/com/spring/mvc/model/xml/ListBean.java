package com.spring.mvc.model.xml;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ListBean {

	private String name;
	@SuppressWarnings("rawtypes")
	private List list;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@SuppressWarnings("rawtypes")
	@XmlElements({
		@XmlElement(name="user", type=UserBean.class),
		@XmlElement(name="account", type=AccountBean.class)
	})
	public List getList() {
		return list;
	}

	@SuppressWarnings("rawtypes")
	public void setList(List list) {
		this.list = list;
	}

}
