package com.spring.mvc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.mvc.model.JsonResult;
import com.spring.mvc.model.User;
import com.spring.mvc.model.UserList;
import com.spring.mvc.model.xml.AccountBean;
import com.spring.mvc.model.xml.Brithday;


@Controller
@RequestMapping("/json")
public class JsonController {

	Logger log = LoggerFactory.getLogger(JsonController.class);
	
	@RequestMapping(method=RequestMethod.POST)
	@ResponseBody
	public JsonResult upload(@RequestBody User u) {
		
		log.info("get json input from request body annotation");
    	log.info(u.getUserName());
        return new JsonResult(true,"return ok");
	}
	
	@RequestMapping("/bean2json")
	public User bean2json() {
		User user = new User();
        user.setAddress("china GuangZhou");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack");
        
        return user;
	}
	
	@RequestMapping("/map2json")
	public Map<String, Object> map2json() {
		
		System.out.println("#################jackson map2json##################");
		Map<String,Object> map = new HashMap<String,Object>();  
		
        User user = new User();
        user.setAddress("china GuangZhou");
        user.setAge(23);
        user.setBirthday(new Date());
        user.setName("jack");
        
        AccountBean bean = new AccountBean();
        bean.setAddress("北京");
        bean.setEmail("email");
        bean.setId(1);
        bean.setName("haha");
        Brithday day = new Brithday();
        day.setBrithday("2010-11-22");
        bean.setBrithday(day);
        
        map.put("user", user);
        map.put("bean", bean);
        return map;
	}
	
	@RequestMapping("/list2json")
	public UserList list2json() {
		System.out.println("#################jackson list2json##################");
		List<User> list = new ArrayList<User>();
		for (int i = 0; i < 3; i++) {
			User user = new User();
	        user.setAddress("北京"+i);
	        user.setAge(23+i);
	        user.setBirthday(new Date());
	        user.setName("admin"+i);
	        
	        list.add(user);
		}
		UserList users = new UserList();
		users.setUserList(list);
		
		return users;
	}
}
