package com.spring.mvc.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.spring.mvc.model.Student;
/**
 * 
 * 测试Json
 *
 */
@Controller
@RequestMapping(value = "/result")
public class ResultController {

	/*
	 * 测试方法
	 */
	@RequestMapping( value="/test_Json", method = RequestMethod.GET)
	public String index() {
		return "testJson/test_Json";
	}
	
	
	@RequestMapping(value = "/helloajax", method = RequestMethod.POST)
	@ResponseBody
	public String helloajax() {
		return "Hello Ajax";
	}

	@RequestMapping(value = "/sum/{a}/{b}", method = RequestMethod.POST)
	@ResponseBody
	public String sum(@PathVariable(value = "a") int a,
			@PathVariable(value = "b") int b) {
		return String.valueOf(a + b);
	}

	@RequestMapping(value = "/json", method = RequestMethod.GET)
	@ResponseBody
	public Student json() {
		return new Student("sv01", "Name 1", null, null);
	}

	@RequestMapping(value = "/listjson", method = RequestMethod.GET)
	@ResponseBody
	public List<Student> listjson() {
		List<Student> ls = new ArrayList<Student>();
		ls.add(new Student("sv01", "Name 1", null, null));
		ls.add(new Student("sv02", "Name 2", null, null));
		ls.add(new Student("sv03", "Name 3", null, null));
		return ls;
	}

}