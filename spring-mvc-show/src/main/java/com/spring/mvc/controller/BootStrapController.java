package com.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value ="/testBootStrap")
public class BootStrapController {
	/**
	 * container 容器
	 * @return
	 */
	@RequestMapping( value="/container", method = RequestMethod.GET)
	public String container() {
		return "testBootstrap/container";
	}
	
	/*
	 * 
	 * 排版
	 */
	@RequestMapping( value="/paiban", method = RequestMethod.GET)
	public String paiban() {
		return "testBootstrap/paiban";
	}
}
