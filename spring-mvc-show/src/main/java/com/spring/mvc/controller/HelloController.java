package com.spring.mvc.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
/*
 *Hello Spring-mvc！ 
 */
@Controller
@RequestMapping("/hello")
public class HelloController {

	@RequestMapping(value="/html",method=RequestMethod.GET)
	public ModelAndView hello() {
		ModelAndView mv = new ModelAndView();
		mv.setViewName("hello");
		return mv;
	}

	@RequestMapping(value="/html1", method=RequestMethod.GET)
	@ResponseBody
	public String show() {
		return "Hello world!";
	}

	@RequestMapping(value="/html2", method=RequestMethod.GET, headers="Accept=text/plain")
	@ResponseBody
	public String simple() {
		return "Hello world revisited!";
	}

}
