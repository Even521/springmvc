package com.spring.mvc.websocket.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.spring.mvc.websocket.consts.Constants;

@Controller
public class LoginController {
	  @RequestMapping("websocket/login")
	    public String login(HttpServletRequest request, @RequestParam("username") String username) {
	        HttpSession session = request.getSession();
	        session.setAttribute(Constants.DEFAULT_SESSION_USERNAME, username);
	        return "redirect:index";
	    }

	    @RequestMapping("websocket/index")
	    public String index() {
	        return "websocket/index";
	    }
	    @RequestMapping("login")
	    public String login() {
	        return "websocket/login";
	    }

}
