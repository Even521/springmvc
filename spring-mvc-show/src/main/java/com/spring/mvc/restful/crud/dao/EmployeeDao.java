package com.spring.mvc.restful.crud.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.spring.mvc.restful.crud.entities.Department;
import com.spring.mvc.restful.crud.entities.Employee;

/**
 * 
 *没有连接数据库，直接写一组模拟数据 
 * 
 *
 */
@Repository
public class EmployeeDao {
	private static Map<Integer, Employee> employees = null;
	
	@Autowired
	private DepartmentDao departmentDao;
	
	static{
		employees = new HashMap<Integer, Employee>();

		employees.put(1001, new Employee(1001, "张三", "aa@163.com", 1, new Department(101, "营业部")));
		employees.put(1002, new Employee(1002, "李四", "bb@163.com", 1, new Department(102, "人事部")));
		employees.put(1003, new Employee(1003, "王小小", "cc@163.com", 0, new Department(103, "财务部")));
		employees.put(1004, new Employee(1004, "tom", "dd@163.com", 0, new Department(104, "销售部")));
		employees.put(1005, new Employee(1005, "王麻子", "ee@163.com", 1, new Department(105, "促销部")));
	}
	
	private static Integer initId = 1006;
	/*
	 * 添加一个员工
	 */
	public void save(Employee employee){
		if(employee.getId() == null){
			employee.setId(initId++);
		}
		
		employee.setDepartment(departmentDao.getDepartment(employee.getDepartment().getId()));
		employees.put(employee.getId(), employee);
	}
	/*
	 * 查询所有员工
	 */
	public Collection<Employee> getAll(){
		return employees.values();
	}
	/*
	 * 
	 */
	public Employee get(Integer id){
		return employees.get(id);
	}
	
	/*
	 * 删除
	 */
	public void delete(Integer id){
		employees.remove(id);
	}
	
}
