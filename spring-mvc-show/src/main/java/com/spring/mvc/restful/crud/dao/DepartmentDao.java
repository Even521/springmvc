package com.spring.mvc.restful.crud.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;

import com.spring.mvc.restful.crud.entities.Department;

/**
 * 
 *没有连接数据库，直接写一组模拟数据 
 * 
 *
 */
@Repository
public class DepartmentDao {
	private static Map<Integer, Department> departments = null;

	static{
		departments = new HashMap<Integer, Department>();
		
		departments.put(101, new Department(101, "营业部"));
		departments.put(102, new Department(102, "人事部"));
		departments.put(103, new Department(103, "财务部"));
		departments.put(104, new Department(104, "销售部"));
		departments.put(105, new Department(105, "促销部"));
	}
	
	public Collection<Department> getDepartments(){
		return departments.values();
	}
	
	public Department getDepartment(Integer id){
		return departments.get(id);
	}

}
