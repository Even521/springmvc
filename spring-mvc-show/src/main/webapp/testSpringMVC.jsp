<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>测试SpringMVC</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/bootstrap/js/jquery-1.11.2.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container ">

   <div class="row">
   <div class="col-md-10  col-md-offset-1 "> 
   <h3>@RequestMapping 映射请求</h3>
   <div class="panel-footer">
  <ul class="h5">
  <li ><p>SpringMVC 使用<code>@RequestMapping</code>注解为控制器指定可以处理那些URL请求.<p></li>
  <li><p>在控制器的<font color="red">类定义及方法定义处</font>都可以标注.<p></li>
  <li>@RequestMapping
  <ul class="h6">
  <li><font color="red">-类定义处</font>：提供初步的请求映射信息。相对于web应用的根目录</li>
  <li><font color="red">-方  法  处</font>：提供进一步的细分映射信息。相对于类定义处的URL。若类定义处未标注<code>@RequestMapping</code>,则方法处标记的URL相对于WEB应用的根目录。</li>
  </ul>
  </li>
  <li>DispatcherSerlet截获请求后，就通过控制器上<code>@RequestMapping</code>提供的映射信息确定请求所有对应的处理方法。</li>
</ul> 
 </div>
    <font color="blue">代码</font>
<pre><code>@Controller
@RequestMapping("/springmvc")
public class SpringMvcTestController {	
	private static final String SUCCESS = "success";
	@RequestMapping("/testRequestMapping")
	@ResponseBody
	public String testRequestMapping() {
		System.out.println("testRequestMapping");
		return SUCCESS;
	}	
}
</code>
</pre>
<div>
</div>
  <div >
   <a  class="btn  btn-primary btn-sm right" style="float:right" href="springmvc/testRequestMapping">Test RequestMapping</a>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-10  col-md-offset-1 ">
   <h3>@RequestMapping 映射请求参数、请求方法或请求头</h3>
   <div class="panel-footer">
  <ul class="h5">
  <li ><p>@RequestMapping除了可以使用<font color="red">请求URL</font>映射请求之外，<font color="red">还可以使用请求方法、请求参数、请求头映射请求</font></p></li>
  <li><p>@RequestMapping的value、method、params、heads分别表示<font color="red">请求URL、请求方法、请求参数、请求头</font>的映射更加精确化。</p></li>
  <li>
  <p>params和headers支持简单的表达式：</p>
  <ul >
  <li>-param1：表示请求必须包含为param1的请求参数</li>
  <li>-!param1:表示请求不能包含名为param1的请求参数</li>
  <li>-param1！=value1：表示请求包含名为param1的请求参数，但其他不能为value1</li>
  <li>-{"param1=value1","param2"}：请求必须包含名为param1和param2的两个请求参数，且param1参数的值必须为value1</li>
  </ul>
  <p></p>
  </li>
  <li><p>Ant风格资源地址支持3种匹配符：</p>
  <ul>
  <li>- ?:匹配文件中的一个字符</li>
  <li>- *:匹配文件名中的任意字符</li>
  <li>- **:**匹配多层路径</li>
  </ul>
   <p></p>
  </li>
  <li>
 <p> @RequestMapping 还支持Ant风格的URL：</p>
  <ul>
  <li>- /uers/*/1:匹配/uers/123/1、/uers/aaa/1等URL</li>
  <li>- /uers/**/1：匹配/uers/123/1、/uers/123/aaa/1等URL</li>
  <li>- /uers/1/1??：匹配/uers/1/111、/uers/1/1ab等URL</li>
  </ul>
   <p></p>
  </li>
  <li><p>PathVariable 映射URL绑定的占位符</p>
  <ul>
  <li>
  带占位的URL是Spring 3.0新增的功能，该功能在SpringMVC向REST目标挺进发展过程中具有里程碑的意义
  </li>
  <li>
    通过<code>@PathVariable</code>可将URL中占位符参数绑定到控制器处理方法的入参中：
  URL中的｛xxx｝占位符可以通过<code>@PathVaiable("xxx")</code>绑定到操作方法的入参中。
  </li>
  </ul>
  </li>
  
</ul> 
 </div>
 <font color="blue">代码</font>
<pre><code>
     /**
      * 常用: 使用 method 属性来指定请求方式
      */
      @RequestMapping(value = "/testMethod", method = RequestMethod.POST)
      public String testMethod() {
	  System.out.println("testMethod");
	  return SUCCESS;
      }

     /**
      * 了解: 可以使用 params 和 headers 来更加精确的映射请求. params 和 headers 支持简单的表达式.
      * 
      * @return
      */
     @RequestMapping(value = "testParamsAndHeaders", params = { "username","age!=10" }, headers = { "Accept-Language=en-US,zh;q=0.8" })
     public String testParamsAndHeaders() {
		System.out.println("testParamsAndHeaders");
		return SUCCESS;
     }

    /**
     * @PathVariable 可以来映射 URL 中的占位符到目标方法的参数中.
     * @param id
     * @return
     */
    @RequestMapping("/testPathVariable/{id}")
    public String testPathVariable(@PathVariable("id") Integer id) {
		System.out.println("testPathVariable: " + id);
		return SUCCESS;
	}

    @RequestMapping("/testAntPath/*/abc")
    public String testAntPath() {
		System.out.println("testAntPath");
		return SUCCESS;
	}
	
   /**
	 * @RequestParam 来映射请求参数. value 值即请求参数的参数名 required 该参数是否必须. 默认为 true
	 *               defaultValue 请求参数的默认值
	 */
	@RequestMapping(value = "/testRequestParam")
	public String testRequestParam(
			@RequestParam(value = "username") String un,
			@RequestParam(value = "age", required = false, defaultValue = "0") int age) {
		System.out.println("testRequestParam, username: " + un + ", age: "
				+ age);
		return SUCCESS;
	}	
	
	/**
	 * 了解: 映射请求头信息 用法同 @RequestParam
	 */
	@RequestMapping("/testRequestHeader")
	public String testRequestHeader(
			@RequestHeader(value = "Accept-Language") String al) {
		System.out.println("testRequestHeader, Accept-Language: " + al);
		return SUCCESS;
	}
	
	/**
	 * 了解:
	 * 
	 * @CookieValue: 映射一个 Cookie 值. 属性同 @RequestParam
	 */
	@RequestMapping("/testCookieValue")
	public String testCookieValue(@CookieValue("JSESSIONID") String sessionId) {
		System.out.println("testCookieValue: sessionId: " + sessionId);
		return SUCCESS;
	}
</code>
</pre>
  <div >
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testMethod">testMethod GET</a>
   <form  action="springmvc/testMethod" method="POST">
		<input style="float:right" class="btn  btn-primary btn-sm" type="submit" value="testMethod POST"/>
	</form>
	 <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testParamsAndHeaders?username=hello&age=1">testParamsAndHeaders</a>
	 <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testAntPath/123/abc">testAntPath</a>
	 <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testPathVariable/1">testPathVariable</a>
	 <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testRequestParam?username=hello&age=11">testRequestParam</a>
	 <a  class="btn  btn-primary btn-sm" style="float:left" href="springmvc/testRequestHeader">testRequestHeader</a>
	 <a  class="btn  btn-primary btn-sm" style="float:left" href="springmvc/testCookieValue">testCookieValue</a>
</div>
</div>
</div>




<div class="row">
   <div class="col-md-10  col-md-offset-1 ">
   <h3>REST</h3>
   <div class="panel-footer">
  <ul class="h5">
  <li > <p>REST:即Representational State Transfer.(资源)表现层状态转化。是目前最流行的一种互联网软件架构。它结构清晰、符合标准、易于理解、扩展方便，所以正得到越来越多网站的采用</p></li>
  <li> <p> 资源(资源)：网络上的一个实体,或者说是网络上的一个具体信息。它可以是一段文本、一张图片、一首歌曲、一种服务，总之就是一个具体的存在。可以用一个URI（统一资源定位符）指向它，每种资源对应一个特定的URI。要获取这个资源，访问它的URI就可以，因此URI即为每一个资源的独一无二的识别符。</p></li>
  <li> <p>表现层(Representation)：把资源具体呈现出来的形式，叫做它的表现层(Representation)。比如：文本可以用txt格式表示，也可以用HTML格式、XML格式、JSON格式表现，甚至可以采用二进制格式。</p></li>
  <li> <p>状态转化(State Transfer)：每发出一个请求，就代表了客户端和服务器的一次交互过程。HTTP协议，就是一个无状态协议，即所有的状态都保存在服务器端。因此，如果客户端想要操作服务器，必须通过某种手段，让服务器端发生"状态转化(State Transfer)"。而这中转化是建立在表现层之上的，所以就是"表现出状态转化"。具体说，就是HTTP协议里面，四个表示操作方式的动词：<code>GET、POST、PUT、DELETE</code>它们分别对应四种基本操作：GET用来获取资源、POST用来新建资源、PUT用来更新资源、DELETE用来删除资源。</p></li>
  <li><p>示例</p>
  <ul>
  <li>- /order/1 HTTP <font color="RED">GET</font>:得到id=1的order</li>
  <li>- /order/1 HTTP <font color="RED">DELETE</font>:删除id=1的order</li>
  <li>- /order/1 HTTP <font color="RED">PUT</font>:更新id=1的order</li>
  <li>- /order/1 HTTP <font color="RED">POST</font>:新增order</li>
  </ul>
  <p></p>
  </li>
   <li><p><font color="RED">HiddenHttpMethodFilter</font>:浏览器form表单只支持GET与POST请求，而DELETE、PUT等method并不支持，Spring3.0以后添加了一个过滤器，可以将这些请求转换为标准的http方法，使得GET、POST、PUT、DELETE请求。</p></li>
</ul> 
 </div>
 <font color="blue">代码</font>
<pre><font color="bule">web.xml</font>
&lt;!-- 配置 org.springframework.web.filter.HiddenHttpMethodFilter: 可以把 POST 请求转为 DELETE 或 POST 请求 &gt;
	&lt;filter&gt;
		&lt;filter-name&gt;HiddenHttpMethodFilter&lt;/filter-name&gt;
		&lt;filter-class&gt;org.springframework.web.filter.HiddenHttpMethodFilter&lt;/filter-class&gt;
	&lt;/filter&gt;
	
	&lt;filter-mapping&gt;
		&lt;filter-name&gt;HiddenHttpMethodFilter&lt;/filter-name&gt;
		&lt;url-pattern&gt;/*&lt;/url-pattern&gt;
	&lt;/filter-mapping&gt;
	<br>
<font color="bule">java code</font>
	@RequestMapping(value = "/testRest/{id}", method = RequestMethod.PUT)
	public String testRestPut(@PathVariable Integer id) {
		System.out.println("testRest Put: " + id);
		return SUCCESS;
	}

	@RequestMapping(value = "/testRest/{id}", method = RequestMethod.DELETE)
	public String testRestDelete(@PathVariable Integer id) {
		System.out.println("testRest Delete: " + id);
		return SUCCESS;
	}

	@RequestMapping(value = "/testRest", method = RequestMethod.POST)
	public String testRest() {
		System.out.println("testRest POST");
		return SUCCESS;
	}

	@RequestMapping(value = "/testRest/{id}", method = RequestMethod.GET)
	public String testRest(@PathVariable Integer id) {
		System.out.println("testRest GET: " + id);
		return SUCCESS;
	}
	
</pre>
  <div >
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testRest/1">Test Rest Get</a>
   <form action="springmvc/testRest" method="post">
		<input class="btn  btn-primary btn-sm" style="float:right" type="submit" value="TestRest POST"/>
	</form>
	<form action="springmvc/testRest/1" method="post">
		<input type="hidden" name="_method" value="DELETE"/>
		<input  class="btn  btn-primary btn-sm" style="float:right" type="submit" value="TestRest DELETE"/>
	</form>
	<form action="springmvc/testRest/1" method="post">
		<input type="hidden" name="_method" value="PUT"/>
		<input class="btn  btn-primary btn-sm" style="float:right" type="submit" value="TestRest PUT"/>
	</form>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-10  col-md-offset-1 ">
   <h3>POJO对象绑定请求参数值</h3>
   <div class="panel-footer">
  <ul class="h5">
  <li ><p>SpringMVC 会按请求参数名和POJP属性名进行自动匹配，自动为该对象填充属性值。支持级联属性。如：dept.deptId、dept.address.tel等</p></li>
  <li><p>Servlet API作为入参：Hander方法可以接受哪些Servlet类型的参数</p>
  <ul>
  <li>HttpServletRequest</li>
  <li>HttpServletRespones</li>
  <li>HttpSeesion</li>
  <li>java.security.Principal</li>
  <li>Locale</li>
  <li>InputStream</li>
  <li>OutputStream</li>
  <li>Reader</li>
  <li>Writer</li>
  </ul>
  </li>
</ul> 
 </div>
 <font color="blue">代码</font>
<pre><code>
      /**
	 * Spring MVC 会按请求参数名和 POJO 属性名进行自动匹配， 自动为该对象填充属性值。支持级联属性。
	 * 如：dept.deptId、dept.address.tel 等
	 */
	@RequestMapping("/testPojo")
	public String testPojo(User user) {
		System.out.println("testPojo: " + user);
		return SUCCESS;
	}
	
	@RequestMapping("/testServletAPI")
	public void testServletAPI(HttpServletRequest request,
			HttpServletResponse response, Writer out) throws IOException {
		System.out.println("testServletAPI, " + request + ", " + response);
		out.write("hello springmvc");
//		return SUCCESS;
	}
</code>
</pre>
  <div class="col-md-4 col-md-offset-1">
   <form action="springmvc/testPojo" method="post">
           <div class="input-group">   
		 <span class="input-group-addon">用户名: </span><input type="text" class="form-control"  name="userName"/>
		<br>
		</div>
		<div class="input-group"> 
		<span class="input-group-addon">密  码: </span><input  type="text" class="form-control" type="password" name="password"/>
		<br>
		
		</div>
		<input  class="btn  btn-primary btn-sm" type="submit" value="testPojo"/>
	    <a class="btn  btn-primary btn-sm" href="springmvc/testServletAPI">testServletAPI</a>
	</form>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-10  col-md-offset-1 ">
   <h3>处理模型数据</h3>
   <div class="panel-footer">
  <ul class="h5">
  <li ><p>SpringMVC 提供了以下几种途径输出模型数据：</p>
  <ul>
  <li><p>ModelAndView:处理方法返回值类型为ModelAndView时，方法体通过该对象添加模型数据<p></li>
  <li><p>Map及Model：入参为org.springframework.ui.Model、org.springframework.ui.ModelMap或java.uti.Map时，处理方法返回时，Map中的数据会自动添加到模型。</p></li>
  <li><p>@SessionAttributes：将模型中的某个属性暂存到HttpSeesion中，以便多个请求之间可以共享这个属性</p></li>
  <li><p>@ModelAttribute：方法入参标注该注解后，入参的对象就会放到数据模型中</p></li>
  </ul>
  </li>
  <li><p>控制器处理方法的返回值如果为ModelAndView，则其既包含视图信息，也包含模型数据信息。</p></li>
  <li><p>添加模型数据：</p>
  <ul>
  <li>ModelAndView addObject(String attributeName,Object attributeValue)</li>
  <li>ModelAndView addAllObject(Map &lt;String,?&gt; modelMap)</li>
  </ul>
  </li>
  <li><p>设置视图</p>
  <ul >
  <li >void setView(View view)</li>
  <li> void setViewName(String viewName)</li>
  </ul>
  </li>
  <li><p>若希望在多个请求之间用某个模型属性数据，则可以在控制器类上标注一个<font color="red">@SeesionAttributes</font>,SpringMVC将在模型中对应的属性暂存到HttpSession中。</p></li>
  <li><p>@SeesionAttributes除了可以通过属性名指定需要放到回话中的属性外，还可以通过模型属性的对象类型指定哪些模型属性需要放到会话中</p>
  <ul>
  <li><p>@SessionAttributes(types=User.class)会将隐含模型中所有类型为User.class的属性添加到回话中。<p></li>
  <li><p>@SessionAttributes(types={User.class,Dept.class})</p></li>
  <li><p>@SessionAttributes(value={"user1","user2"})</p></li>
  <li><p>@SessionAttributes(value={"user1","user2"},types={dept.class})</p></li>
  </ul>
  </li> 
  <li><p>在方法定义上使用@ModelAttribute注解：Springmvc在调用目标处理方法前，会先逐个调用在方法级上标注了@ModelAttribute的方法。</p></li>
  <li><p>在方法的入参前使用@ModelAttribute注解：</p>
  <ul>
  <li><p>可以从隐含对象中获取的模型数据中获取对象，在将请求参数绑定到对象中，再传入入参<p></li>
  <li><p>将方法入参对象添加到模型中</p></li>
  </ul>
  </li>
  <li><p>如果在处理类定义处标注了@SessionAttribute（"xxx"）,则尝试从中会话中获取该属性，并将其赋给该入参，然后再用请求消息填充该入参对象。如果在会话中找不到对应的属性，则抛出HttpSessionRequiredException异常</p></li>
</ul> 
 </div>
 <font color="blue">代码</font>
<pre><code>@RequestMapping("/testModelAndView")
	public ModelAndView testModelAndView(){
		String viewName = SUCCESS;
		ModelAndView modelAndView = new ModelAndView(viewName);
		
		//添加模型数据到 ModelAndView 中.
		modelAndView.addObject("time", new Date());
		
		return modelAndView;
	}
	
	/**
	 * 目标方法可以添加 Map 类型(实际上也可以是 Model 类型或 ModelMap 类型)的参数. 
	 * @param map
	 * @return
	 */
	@RequestMapping("/testMap")
	public String testMap(Map&lt;String&gt;, Object map){
		System.out.println(map.getClass().getName()); 
		map.put("names", Arrays.asList("Tom", "Jerry", "Mike"));
		return SUCCESS;
	}
	
	/**
	 * @SessionAttributes 除了可以通过属性名指定需要放到会话中的属性外(实际上使用的是 value 属性值),
	 * 还可以通过模型属性的对象类型指定哪些模型属性需要放到会话中(实际上使用的是 types 属性值)
	 * 
	 * 注意: 该注解只能放在类的上面. 而不能修饰放方法. 
	 */
	@RequestMapping("/testSessionAttributes")
	public String testSessionAttributes(Map&lt;String&gt;, Object map){
		User user = new User("1", "tom", "123", "chengdu","boy", 17);
		map.put("user", user);
		map.put("school", "蓝翔");
		return SUCCESS;
	}
	
	/**
	 * 运行流程:
	 * 1. 执行 @ModelAttribute 注解修饰的方法: 从数据库中取出对象, 把对象放入到了 Map 中. 键为: user
	 * 2. SpringMVC 从 Map 中取出 User 对象, 并把表单的请求参数赋给该 User 对象的对应属性.
	 * 3. SpringMVC 把上述对象传入目标方法的参数. 
	 * 
	 * 注意: 在 @ModelAttribute 修饰的方法中, 放入到 Map 时的键需要和目标方法入参类型的第一个字母小写的字符串一致!
	 * 
	 * SpringMVC 确定目标方法 POJO 类型入参的过程
	 * 1. 确定一个 key:
	 * 1). 若目标方法的 POJO 类型的参数木有使用 @ModelAttribute 作为修饰, 则 key 为 POJO 类名第一个字母的小写
	 * 2). 若使用了  @ModelAttribute 来修饰, 则 key 为 @ModelAttribute 注解的 value 属性值. 
	 * 2. 在 implicitModel 中查找 key 对应的对象, 若存在, 则作为入参传入
	 * 1). 若在 @ModelAttribute 标记的方法中在 Map 中保存过, 且 key 和 1 确定的 key 一致, 则会获取到. 
	 * 3. 若 implicitModel 中不存在 key 对应的对象, 则检查当前的 Handler 是否使用 @SessionAttributes 注解修饰, 
	 * 若使用了该注解, 且 @SessionAttributes 注解的 value 属性值中包含了 key, 则会从 HttpSession 中来获取 key 所
	 * 对应的 value 值, 若存在则直接传入到目标方法的入参中. 若不存在则将抛出异常. 
	 * 4. 若 Handler 没有标识 @SessionAttributes 注解或 @SessionAttributes 注解的 value 值中不包含 key, 则
	 * 会通过反射来创建 POJO 类型的参数, 传入为目标方法的参数
	 * 5. SpringMVC 会把 key 和 POJO 类型的对象保存到 implicitModel 中, 进而会保存到 request 中. 
	 * 
	 * 源代码分析的流程
	 * 1. 调用 @ModelAttribute 注解修饰的方法. 实际上把 @ModelAttribute 方法中 Map 中的数据放在了 implicitModel 中.
	 * 2. 解析请求处理器的目标参数, 实际上该目标参数来自于 WebDataBinder 对象的 target 属性
	 * 1). 创建 WebDataBinder 对象:
	 * ①. 确定 objectName 属性: 若传入的 attrName 属性值为 "", 则 objectName 为类名第一个字母小写. 
	 * *注意: attrName. 若目标方法的 POJO 属性使用了 @ModelAttribute 来修饰, 则 attrName 值即为 @ModelAttribute 
	 * 的 value 属性值 
	 * 
	 * ②. 确定 target 属性:
	 * 	> 在 implicitModel 中查找 attrName 对应的属性值. 若存在, ok
	 * 	> *若不存在: 则验证当前 Handler 是否使用了 @SessionAttributes 进行修饰, 若使用了, 则尝试从 Session 中
	 * 获取 attrName 所对应的属性值. 若 session 中没有对应的属性值, 则抛出了异常. 
	 * 	> 若 Handler 没有使用 @SessionAttributes 进行修饰, 或 @SessionAttributes 中没有使用 value 值指定的 key
	 * 和 attrName 相匹配, 则通过反射创建了 POJO 对象
	 * 
	 * 2). SpringMVC 把表单的请求参数赋给了 WebDataBinder 的 target 对应的属性. 
	 * 3). *SpringMVC 会把 WebDataBinder 的 attrName 和 target 给到 implicitModel. 
	 * 近而传到 request 域对象中. 
	 * 4). 把 WebDataBinder 的 target 作为参数传递给目标方法的入参. 
	 */
	@RequestMapping("/testModelAttribute")
	public String testModelAttribute(User user){
		System.out.println("修改: " + user);
		return SUCCESS;
	}
	
	/**
	 * 1. 有 @ModelAttribute 标记的方法, 会在每个目标方法执行之前被 SpringMVC 调用! 
	 * 2. @ModelAttribute 注解也可以来修饰目标方法 POJO 类型的入参, 其 value 属性值有如下的作用:
	 * 1). SpringMVC 会使用 value 属性值在 implicitModel 中查找对应的对象, 若存在则会直接传入到目标方法的入参中.
	 * 2). SpringMVC 会一 value 为 key, POJO 类型的对象为 value, 存入到 request 中. 
	 */
	@ModelAttribute
	public void getUser(@RequestParam(value="id",required=false) Integer id, 
			Map&lt;String, Object&gt; map){
		System.out.println("modelAttribute method");
		if(id != null){
			//模拟从数据库中获取对象
			User user = new User("1", "tom", "123", "chengdu","boy", 17);
			System.out.println("从数据库中获取一个对象: " + user);
			
			map.put("user", user);
		}
	}
  
  	@RequestMapping("/testViewAndViewResolver")
	public String testViewAndViewResolver(){
		System.out.println("testViewAndViewResolver");
		return SUCCESS;
	}
	
	/*
	 *自定义视图
	 */
	@RequestMapping("/testView")
	public String testView(){
		System.out.println("testView");
		return "helloView";
	}
	
	@RequestMapping("/testRedirect")
	public String testRedirect(){
		System.out.println("testRedirect");
		return "redirect:/index.jsp";
	}
</code>
</pre>
 <!--  
		模拟修改操作
		1. 原始数据为: 1, Tom, 123456,tom@atguigu.com,12
		2. 密码不能被修改.
		3. 表单回显, 模拟操作直接在表单填写对应的属性值
	-->
	<form action="springmvc/testModelAttribute" method="Post">
	 
		  <input type="hidden" name="id" value="1"/>
		 
		<div class="input-group"><span class="input-group-addon">  username: </span><input type="text" class="form-control" name="username" value="Tom"/>
		<br>
        </div>

		<div class="input-group"><span class="input-group-addon">email:</span> <input type="text" class="form-control" name="email" value="tom@atguigu.com"/>
		<br>
        </div>
 
		 <div class="input-group"> <span class="input-group-addon">
		age:</span> <input type="text" class="form-control" name="age" value="12"/>
		<br>
        </div>
		<input type="submit" class="btn  btn-primary btn-sm" value="Submit"/>
	</form>

  <div >
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testRedirect">testRedirect</a>
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testView">testView</a>
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testViewAndViewResolver">testViewAndViewResolver</a>
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testModelAndView">testModelAndView</a>
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testMap">testMap</a>
   <a  class="btn  btn-primary btn-sm" style="float:right" href="springmvc/testSessionAttributes">testSessionAttributes</a>
</div>
</div>
</div>

<div class="row">
   <div class="col-md-10  col-md-offset-1 ">
   <h3></h3>
   <div class="panel-footer">
  <ul class="h5">
  <li ><p></p></li>
  <li>
  <ul >
  <li ></li>
  </ul>
  </li>
</ul> 
 </div>
 <font color="blue">代码</font>
<pre><code>

</code>
</pre>
  <div >
   <a  class="btn  btn-primary btn-sm" style="float:right" href=""></a>
</div>
</div>
</div>

</div>
</body>
</html>