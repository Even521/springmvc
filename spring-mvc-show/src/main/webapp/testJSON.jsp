<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>JSON test</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-1.11.2.min.js" ></script>

<script type="text/javascript">
$(document).ready(function() {  
    $("#login").click(function() {  
    	doTestJson();
    });  
}); 

	function doTestJson(){ 
		//发送的对象不要转成JSON字符串，直接发送JSON对象即可，否则就415错误！
		var mydata = '{"userName":"' + $('#userName').val() + '","password":"'  
        	+ $('#password').val() + '","mobileNO":"' + $('#mobileNO').val() + '"}';  
		
        	$.ajax({  
		    type : 'POST',  
		    contentType : 'application/json',  
		    url : 'json.action',  
		    processData : false,  
		    dataType : 'json',  
		    data : mydata,  
		    success : function(data) {  
		        alert(data.success);  
		    },  
		    error : function() {  
		        alert('Err...');  
		    }
		});
	}
</script>
</head>

<body>
	<table>  
    <tr>  
        <td>userName</td>  
        <td><input id="userName" value="100" /></td>  
    </tr>  
    <tr>  
        <td>password</td>  
        <td><input id="password" value="snowolf" /></td>  
    </tr>  
    <tr>  
        <td>mobileNO</td>  
        <td><input id="mobileNO" value="true" /></td>  
    </tr>  
    <tr>  
        <td><input type="button" id="login" value="test" /></td>  
    </tr>  
</table> 
</body>
</html>
