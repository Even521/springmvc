<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>export</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

<script type="text/javascript">
	$(document).ready(function() {
		$("#exec").click(function() {
			//获取下拉框的值
			var titlesValue = "";//$("#columns").find("option:selected").text();  	  		   
			$("#columns").find("option:selected").each(function() { //由于复选框一般选中的是多个,所以可以循环输出
				titlesValue += ($(this).text()) + ",";
			});
			var names = $("#columns").val();
			$("#colums").val(names);
			$("#titles").val(titlesValue);
		});
	});
</script>

</head>

<body>
	<div style="border: 1px solid #ccc; width: 50%;height:200px;align:center;margin-top:200px;margin-left:300px;padding:50px;">
		<form action="${pageContext.request.contextPath}/export/excel" method="post">
			<input type="submit" value="使用POI导出Excel"><br>
		</form>
		<hr>
		<br>
		<form method="post" action="${pageContext.request.contextPath}/export/jxlExcel">
			<select id="columns" multiple="multiple" style="width:100px;height:120px;">
				<option value="id">ID</option>
				<option value="name">姓名</option>
				<option value="sex">性别</option>
				<option value="age">年龄</option>
				<option value="password">密码</option>
				<option value="address">地址</option>
			</select> 
			<input type="hidden" id="titles" name="titles"> 
			<input type="hidden" id="colums" name="colums"> 
			<input type="submit" id="exec" value="使用JXL导出Excel"><br>
		</form>
		<hr>
		<br>
		<form action="${pageContext.request.contextPath}/export/pdf" method="post">
			<input type="submit" value="导出PDF">
		</form>
	</div>
</body>
</html>
