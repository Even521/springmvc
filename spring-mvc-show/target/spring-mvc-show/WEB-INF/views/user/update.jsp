<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>user modify</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

</head>

<body>
	<h2>修改用户信息页面</h2> <br>
        <!-- 此时没有写action,直接提交会提交给/update -->
        <sf:form method="post" modelAttribute="user">
            姓名:<sf:input path="userName"/>  <sf:errors path="userName" /> <br/>
            密码:<sf:password path="password"/> <sf:errors path="password" /> <br/>
            邮箱:<sf:input path="email"/> <sf:errors path="email" /> <br/>
            <input type="submit" value="修改" />
        </sf:form>
</body>
</html>
