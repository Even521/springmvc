<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page isELIgnored="false" %>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>user list</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">

</head>

<body>
	<h2>用户信息展示</h2>   <p><a href="<%=basePath %>/user/add">添加信息</a></p>
	<c:forEach items="${users}" var="usermap">
        姓名:     <a href="<%=basePath %>/user/${usermap.value.userName }">${usermap.value.userName}  </a>
        密码:    ${usermap.value.password }
        邮箱:    ${usermap.value.email }
        <a href="<%=basePath %>/user/${usermap.value.userName }/update">修改</a>
        <a href="<%=basePath %>/user/${usermap.value.userName }/delete">删除</a>
        <br/>
    </c:forEach>
</body>
</html>
