<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>

<script type="text/javascript"
	src="${pageContext.servletContext.contextPath}/bootstrap/js/jquery-1.11.2.min.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
		$('.clickme').click(function() {
			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/result/helloajax',
				success : function(data) {
					$('.result').html(data);
				}
			});
		});
		$('.sum').click(function() {
			$.ajax({
				type : 'POST',
				url : '${pageContext.request.contextPath}/result/sum/1/2',
				success : function(data) {
					$('.result').html(data);
				}
			});
		});
		$('.json').click(function() {
			$.ajax({
				type : 'GET',
				headers: { 
			        Accept : "application/json; charset=utf-8",
			        "Content-Type": "application/json; charset=utf-8"
			    },
				url : '${pageContext.request.contextPath}/result/json',
				success : function(data) {
					$('.result').html(data.name + ' - ' + data.sex+'-'+data.date+''+data.count);
				}
			});
		});
		$('.listjson').click(function() {
			$.ajax({
				type : 'GET',
				headers: { 
			        Accept : "application/json; charset=utf-8",
			        "Content-Type": "application/json; charset=utf-8"
			    },
				url : '${pageContext.request.contextPath}/result/listjson',
				success : function(data) {
					var result = "";
					for(var i=0; i<data.length;i++)
						result += "<br>" + data[i].name + ' - ' + data[i].sex+'-'+data[i].date+'-'+data[i].count;
					$('.result').html(result);
				}
			});
		});
	});
</script>

</head>
<body>

	<input type="button" class="clickme" value="Click me">
	<input type="button" class="sum" value="Sum">
	<input type="button" class="json" value="JSON">
	<input type="button" class="listjson" value="List JSON">
	<div class="result"></div>

</body>
</html>
