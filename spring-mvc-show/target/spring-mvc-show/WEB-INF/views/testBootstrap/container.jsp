<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>container 容器</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/bootstrap/css/bootstrap-theme.min.css">
<script src="${pageContext.request.contextPath}/bootstrap/js/jquery-1.11.2.min.js"></script>
<script src="${pageContext.request.contextPath}/bootstrap/js/bootstrap.min.js"></script>
<style type="text/css">
	.c{
		border: 1px solid gray;
	}
	.tips { color:#f00; font-size:95%; }
</style>
</head>
<body>

<div class="container">
   <div class="row">
   <div class="col-md-10"></div>
   <div class="col-md-2 h4">
   <a href="http://v3.bootcss.com/css/#grid-example-wrapping">官网API地址</a>
   </div>
   </div>
  <h5 >.container 类用于固定宽度并支持响应式布局的容器。...
    用于网页布局</h5>
  
  <h5 class="tips">.row默认12列</h5>
  <div class="row">
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
  <div class="col-md-1 c">.col-md-1</div>
</div>
<h5 class="tips">如果在一个 .row 内包含的列（column）大于12个，包含多余列（column）的元素将作为一个整体单元被另起一行排列。</h5>
<div class="row">
  <div class="col-xs-9 c" >.col-xs-9</div>
  <div class="col-xs-4 c">.col-xs-4<br>Since 9 + 4 = 13 &gt; 12, this 4-column-wide div gets wrapped onto a new line as one contiguous unit.</div>
  <div class="col-xs-6 c">.col-xs-6<br>Subsequent columns continue along the new line.</div>
</div>
<h5 class="tips">col-md-8 表示占8列<br>
</h5>
<div class="row">
  <div class="col-md-8 c">.col-md-8</div>
  <div class="col-md-4 c">.col-md-4</div>
</div>
<h5 class="tips">col-md-4 表示占4列<br>
</h5>
<div class="row">
  <div class="col-md-4 c">.col-md-4</div>
  <div class="col-md-4 c">.col-md-4</div>
  <div class="col-md-4 c">.col-md-4</div>
</div>
<h5 class="tips">col-md-6 表示占6列<br>
</h5>
<div class="row">
  <div class="col-md-6 c">.col-md-6</div>
  <div class="col-md-6 c">.col-md-6</div>
</div>
<h5 class="tips">使用 .col-md-offset-* 类可以将列向右侧偏移。这些类实际是通过使用 * 选择器为当前元素增加了左侧的边距（margin）。例如，.col-md-offset-4 类将 .col-md-4 元素向右侧偏移了4个列（column）的宽度。</h5>
<div class="row ">
  <div class="col-md-4 c">.col-md-4</div>
  <div class="col-md-4 col-md-offset-4 c">.col-md-4 .col-md-offset-4</div>
</div>
<div class="row ">
  <div class="col-md-3 col-md-offset-3 c">.col-md-3 .col-md-offset-3</div>
  <div class="col-md-3 col-md-offset-3 c">.col-md-3 .col-md-offset-3</div>
</div>
<div class="row">
  <div class="col-md-6 col-md-offset-3 c">.col-md-6 .col-md-offset-3</div>
</div>
<h3>嵌套列</h3>
<h5 class="tips">为了使用内置的栅格系统将内容再次嵌套，可以通过添加一个新的 .row 元素和一系列 .col-sm-* 元素到已经存在的 .col-sm-* 元素内。被嵌套的行（row）所包含的列（column）的个数不能超过12（其实，没有要求你必须占满12列）。
</h5>
<div class="row">
  <div class="col-sm-10 c">
    Level 1: .col-sm-9
    <div class="row">
      <div class="col-xs-8 col-sm-6 c">
        Level 2: .col-xs-8 .col-sm-6
      </div>
      <div class="col-xs-4 col-sm-6 c">
        Level 2: .col-xs-4 .col-sm-6
      </div>
    </div>
  </div>
</div>

<h3>排序列</h3>
<h5 class="tips">通过使用 .col-md-push-* 和 .col-md-pull-* 类就可以很容易的改变列（column）的顺序。</h5>
<div class="row">
  <div class="col-md-9 col-md-push-3 c">.col-md-9 .col-md-push-3</div>
  <div class="col-md-3 col-md-pull-9 c">.col-md-3 .col-md-pull-9</div>
</div>
</div>

<div class="container-fluid">
   <h5 class="tips">.container-fluid 类用于 100% 宽度，占据全部视口（viewport）的容器...
  一般用于移动端</h5>
  <div class="row">
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	  <div class="col-md-1 c">.col-md-1</div>
	</div>
</div>
</body>
</html>